import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { LoginRequest, LoginResponse } from '../../model/login.model';

@Injectable({
    providedIn: 'root'
})
export class AuthentificationService {

    constructor(private http: HttpClient) {
    }

    // ADD
    public login(request: LoginRequest): Observable<LoginResponse> {
        return this.http
            .post<LoginResponse>(environment.firebaseAuthURL, request)
            .pipe(
                tap((loginResponse: LoginResponse) => this.setSession(loginResponse))
            );
    }

    private setSession(loginResponse: LoginResponse) {
        const expiresAt = new Date(Date.now() + loginResponse.expiresIn * 1000);

        sessionStorage.setItem('id_token', loginResponse.idToken);
        sessionStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
    }

    public logout(): void {
        sessionStorage.removeItem('id_token');
        sessionStorage.removeItem('expires_at');
    }

    public isLoggedIn(): boolean {
        return Date.now() < this.getExpiration();
    }

    public isLoggedOut(): boolean {
        return !this.isLoggedIn();
    }

    public getExpiration(): number {
        const expiration = sessionStorage.getItem('expires_at');
        const expiresAt = JSON.parse(expiration);
        return expiresAt;
    }
}
