import { HttpClient, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Image } from '../../model/image.model';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor(private http: HttpClient) {
    }

    // UPLOAD IMAGE GALLERIE
    public uploadImageGalerie(image: FormData): Observable<HttpEvent<Image>> {
        return this.http.post<Image>(environment.firebaseHttpURL + '/uploadImageGalerie',
            image,
            {
                reportProgress: true,
                observe: 'events'
            });
    }

    // GET ALL IMAGE URL
    public getAllImageGalerie(): Observable<Image[]> {
        return this.http.get<Image[]>(environment.firebaseHttpURL + '/getAllImageGalerie', {});
    }

    // UPLOAD IMAGE NEWS
    public uploadImageNews(image: FormData): Observable<HttpEvent<Image>> {
        return this.http.post<Image>(environment.firebaseHttpURL + '/uploadImageNews',
            image,
            {
                reportProgress: true,
                observe: 'events'
            });
    }

    // GET ALL IMAGE URL
    public getAllImageNews(): Observable<Image[]> {
        return this.http.get<Image[]>(environment.firebaseHttpURL + '/getAllImageNews', {});
    }

}
