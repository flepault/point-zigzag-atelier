import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { ActivatedRoute, Event, NavigationEnd, Router } from '@angular/router';

@Injectable({
    providedIn: 'root',
})
export class EventRouteService {

    private lastChild$: BehaviorSubject<ActivatedRoute> = new BehaviorSubject<ActivatedRoute>(undefined);

    constructor(private router: Router, private activatedRoute: ActivatedRoute) {
        this.router.events
            .pipe(
                filter((event: Event) => event instanceof NavigationEnd),
                map(() => this.activatedRoute.root.firstChild),
                filter(firstChild => !!firstChild)
            )
            .subscribe(firstChild => {

                let child = firstChild;
                while (!!child.firstChild) {
                    child = child.firstChild;
                }
                this.lastChild$.next(child);
            });
    }

    getTitre$(): Observable<string> {
        return this.lastChild$.pipe(
            filter(value => !!value),
            switchMap(value => value.data),
            map(data => data.titre)
        );
    }
}
