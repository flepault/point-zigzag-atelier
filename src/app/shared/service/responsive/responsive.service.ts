import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ResponsiveService {

  private readonly isHandset$: Observable<boolean>;

  constructor(
      private breakpointObserver: BreakpointObserver) {
    this.isHandset$ = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => {
              console.log(result);
              return result.matches;
            }),
            shareReplay()
        );
  }

  public isMobile(): Observable<boolean> {
    return this.isHandset$;
  }

}
