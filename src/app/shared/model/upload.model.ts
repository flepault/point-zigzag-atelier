export interface UploadFileResponseDTO {
    url: string;
    thumbnailUrl: string;
}
