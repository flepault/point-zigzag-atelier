import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { LayoutModule } from './layout/layout.module';
import { AppRoutingModule } from './app.routing.module';
import { StateModule } from './state/state.module';
import { FirebaseState } from './state/firebase/firebase.state';
import { environment } from '../environments/environment';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { AgmCoreModule } from '@agm/core';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        StateModule,
        SharedModule,
        LayoutModule,
        AppRoutingModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAfstNBanYLTrxPyqyb1TGbLFPOW-sSMdA'
        }),
        [NgxsModule.forRoot([FirebaseState], {developmentMode: !environment.production})],
        NgxsReduxDevtoolsPluginModule.forRoot({disabled: environment.production})
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
