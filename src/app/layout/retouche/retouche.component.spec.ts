import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RetoucheComponent } from './retouche.component';

describe('WelcomeComponent', () => {
  let component: RetoucheComponent;
  let fixture: ComponentFixture<RetoucheComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RetoucheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetoucheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
