import {
  Component,
  OnInit
} from '@angular/core';
import { ResponsiveService } from '../../shared/service/responsive/responsive.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-retouche',
    templateUrl: './retouche.component.html',
    styleUrls: ['./retouche.component.css']
})
export class RetoucheComponent implements OnInit {

    isMobile$: Observable<boolean>;
    retouchePath = 'https://firebasestorage.googleapis.com/v0/b/point-zigzag-atelier.appspot.com/o/photos%2Fretouche%2FFICHE%20PRIX%20RETOUCHE.jpg?alt=media&token=1f5e6803-a7f8-45b2-89dd-8396e1332137';

    constructor(private responsiveService: ResponsiveService) {
    }

    ngOnInit(): void {
        this.isMobile$ = this.responsiveService.isMobile();
    }

}
