import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import { Login } from '../../../state/firebase/firebase.action';
import { LoginComplete, LoginError } from '../../../state/firebase/firebase.event';

@Component({
    selector: 'app-login-modal',
    templateUrl: './login-modal.component.html',
    styleUrls: ['./login-modal.component.css']
})
export class LoginModalComponent implements OnInit {

    public loginForm: FormGroup;

    constructor(public dialogRef: MatDialogRef<LoginModalComponent>, private store: Store, private actions: Actions) {
        this.loginForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        });
    }

    ngOnInit(): void {
        this.subscribeErrorLogin();
        this.subscribeSuccessLogin();
    }

    private subscribeErrorLogin(): void {
        this.actions.pipe(ofActionDispatched(LoginError))
            .subscribe((loginError: LoginError) => {
                this.loginForm.setErrors({
                    serverError: loginError.err
                });
            });
    }

    private subscribeSuccessLogin(): void {
        this.actions.pipe(ofActionDispatched(LoginComplete))
            .subscribe(() => {
                this.dialogRef.close();
            });
    }

    public annuler(): void {
        this.dialogRef.close();
    }

    public valider(): void {
        if (this.loginForm.valid) {
            this.loginForm.setErrors({});
            this.store.dispatch(new Login(this.loginForm.controls.email.value, this.loginForm.controls.password.value));
        }
    }

}
