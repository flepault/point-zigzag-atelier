import { NgModule } from '@angular/core';

import { StorageService } from '../../shared/service/storage/storage.service';
import { SharedModule } from '../../shared/shared.module';
import { AdminActionComponent } from './admin-action/admin-action.component';
import { DropZoneModalComponent } from './drop-zone-modal/drop-zone-modal.component';
import { LoginModalComponent } from './login-modal/login-modal.component';

@NgModule({
    declarations: [
        AdminActionComponent,
        DropZoneModalComponent,
        LoginModalComponent
    ],
    exports: [
        AdminActionComponent,
        DropZoneModalComponent
    ],
    imports: [SharedModule],
    providers: [StorageService]
})
export class AdminModule {
}
