import {
    HttpErrorResponse,
    HttpEvent,
    HttpEventType
} from '@angular/common/http';
import {
    Component,
    ElementRef,
    Inject,
    ViewChild
} from '@angular/core';
import { Store } from '@ngxs/store';
import {
    Observable,
    of,
    OperatorFunction
} from 'rxjs';
import {
    catchError,
    map
} from 'rxjs/operators';
import { UploadFileResponseDTO } from '../../../shared/model/upload.model';
import { StorageService } from '../../../shared/service/storage/storage.service';
import {
    UpdateGallery,
    UpdateNews
} from '../../../state/firebase/firebase.action';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface FileUpload {
    file: File;
    inProgress: boolean;
    progress: number;
}

export interface DialogData {
    uploadNews: boolean;
    uploadGallery: boolean;
}

@Component({
    selector: 'app-drop-zone-modal',
    templateUrl: './drop-zone-modal.component.html',
    styleUrls: ['./drop-zone-modal.component.css']
})
export class DropZoneModalComponent {

    @ViewChild('fileUpload', {static: false})
    fileUpload: ElementRef;
    files = [];

    uploadGallery = false;
    uploadNews = false;

    constructor(private storageService: StorageService,
                private store: Store,
                @Inject(MAT_DIALOG_DATA) public data: DialogData) {
                this.uploadGallery = data.uploadGallery;
                this.uploadNews = data.uploadNews;
    }

    private uploadProgress(fileUpload: FileUpload) {
        return map((event: HttpEvent<UploadFileResponseDTO>) => {
            switch (event.type) {
                case HttpEventType.UploadProgress:
                    fileUpload.progress = Math.round(event.loaded * 100 / event.total);
                    break;
                case HttpEventType.Response:
                    return event;
            }
        }), catchError((error: HttpErrorResponse) => {
            fileUpload.inProgress = false;
            return of(`${fileUpload.file.name} upload failed: ${error}`);
        });
    }

    private uploadImageNews(fileUpload: FileUpload, image: FormData): void {
        this.storageService.uploadImageNews(image)
            .pipe(this.uploadProgress(fileUpload))
            .subscribe((event: any) => {
                if (typeof (event) === 'object') {
                    return this.store.dispatch(new UpdateNews(event.body));
                }
            });
    }

    private uploadImageGallery(fileUpload: FileUpload, image: FormData): void {
        this.storageService.uploadImageGalerie(image)
            .pipe(this.uploadProgress(fileUpload))
            .subscribe((event: any) => {
                if (typeof (event) === 'object') {
                    return this.store.dispatch(new UpdateGallery(event.body));
                }
            });
    }

    private uploadFile(fileUpload: FileUpload): void {

        const image: FormData = new FormData();
        image.append(
            fileUpload.file.name,
            fileUpload.file,
            fileUpload.file.name
        );

        fileUpload.inProgress = true;

        if (this.uploadGallery) {
            this.uploadImageGallery(fileUpload, image);
        } else if (this.uploadNews) {
            this.uploadImageNews(fileUpload, image);
        }

    }

    private uploadFiles() {
        this.fileUpload.nativeElement.value = '';
        this.files.forEach(file => {
            this.uploadFile(file);
        });
    }

    public onClick(): void {
        const fileUpload = this.fileUpload.nativeElement;
        fileUpload.onchange = () => {
            for (let i = 0, len = fileUpload.files.length; i < len; i++) {
                this.files.push({file: fileUpload.files[i], inProgress: false, progress: 0});
            }
            this.uploadFiles();
        };
        fileUpload.click();
    }

}
