import {
    Component, ElementRef,
    OnInit, ViewChild
} from '@angular/core';
import { ResponsiveService } from '../../shared/service/responsive/responsive.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-confection',
    templateUrl: './confection.component.html',
    styleUrls: ['./confection.component.css']
})
export class ConfectionComponent implements OnInit {

    @ViewChild('focusElement', {static: false}) focusElement: ElementRef;

    page = 1;
    totalPages = 0;
    isMobile$: Observable<boolean>;
    confectionPath = '../assets/images/CATALOGUE PRINTEMPS ETE 2021.pdf';
    zoomScale = 'page-width';

    constructor(private responsiveService: ResponsiveService) {
    }

    ngOnInit(): void {
        this.isMobile$ = this.responsiveService.isMobile();
    }

    prev(): void {
        this.page--;
    }

    next(): void {
        this.page++;
    }

    callBackFn(event) {
        this.totalPages = event._pdfInfo.numPages;
    }

}
