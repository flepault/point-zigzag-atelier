import { Component, OnInit } from '@angular/core';
import SwiperCore, {
  Autoplay,
  Pagination
} from 'swiper';
import { Store } from '@ngxs/store';
import { Image } from '../../shared/model/image.model';
import { FirebaseState } from '../../state/firebase/firebase.state';

SwiperCore.use([Autoplay, Pagination]);

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  alt = '../assets/images/404.png';

  images: Image[] = [];

  constructor(public store: Store) { }

  ngOnInit(): void {
    this.subsribeNews();
  }

  private subsribeNews() {
    this.store
        .select(FirebaseState.news)
        .subscribe((images: Image[]) => (this.images = images.map((image) => ({
          ...image,
          selected: false
        }))));
  }
}
