import { Component, OnInit } from '@angular/core';
import { ResponsiveService } from '../../shared/service/responsive/responsive.service';
import { Observable } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Actions, Store } from '@ngxs/store';

@Component({
    selector: 'app-broderie',
    templateUrl: './broderie.component.html',
    styleUrls: ['./broderie.component.css']
})
export class BroderieComponent implements OnInit {

    isMobile$: Observable<boolean>;
    broderiePath = [
        'https://firebasestorage.googleapis.com/v0/b/point-zigzag-atelier.appspot.com/o/photos%2Fbroderie%2FBANNIERE%20CARRE%20BRODERIE.jpg?alt=media&token=8a8a30a2-ffdb-463e-b246-f94afebe44ee'
        , 'https://firebasestorage.googleapis.com/v0/b/point-zigzag-atelier.appspot.com/o/photos%2Fbroderie%2FBANIERE%20BRODERIE.jpg?alt=media&token=d50756e0-00e2-4913-a6f5-732a5ddf7906',
        '../assets/images/comingsoon.png'
    ];

    public broderieForm: FormGroup;

    constructor(private responsiveService: ResponsiveService) {
        this.broderieForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        });
    }

    ngOnInit(): void {
        this.isMobile$ = this.responsiveService.isMobile();
    }

    public valider(): void {
        if (this.broderieForm.valid) {
            this.broderieForm.setErrors({});
            // this.store.dispatch(new Login(this.loginForm.controls.email.value, this.loginForm.controls.password.value));
        }
    }
}
