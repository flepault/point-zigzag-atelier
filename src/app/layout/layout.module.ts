import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {NavigationComponent} from './navigation/navigation.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import { ConfectionComponent } from './confection/confection.component';
import { ShopComponent } from './shop/shop.component';
import { AppRoutingModule } from '../app.routing.module';
import { MatCardModule } from '@angular/material/card';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WelcomeComponent } from './welcome/welcome.component';
import { SwiperModule } from 'swiper/angular';
import { AdminModule } from './admin/admin.module';
import { ContactComponent } from './contact/contact.component';
import { BroderieComponent } from './broderie/broderie.component';
import { RetoucheComponent } from './retouche/retouche.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AgmCoreModule } from '@agm/core';


@NgModule({
    declarations: [
        NavigationComponent,
        WelcomeComponent,
        ConfectionComponent,
        BroderieComponent,
        RetoucheComponent,
        ContactComponent,
        ShopComponent
    ],
    imports: [
        SwiperModule,
        SharedModule,
        AppRoutingModule,
        MatCardModule,
        BrowserModule,
        BrowserAnimationsModule,
        FlexLayoutModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        AdminModule,
        PdfViewerModule,
        AgmCoreModule
    ],
    exports: [
        NavigationComponent
    ]
})
export class LayoutModule {
}
