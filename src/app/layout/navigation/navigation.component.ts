import {
    Component,
    OnInit
} from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ResponsiveService } from '../../shared/service/responsive/responsive.service';
import { environment } from '../../../environments/environment';
import { Store } from '@ngxs/store';
import { LoadGallery, LoadNews } from '../../state/firebase/firebase.action';
import { EventRouteService } from '../../shared/service/route/event-route.service';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

    logoPath = '../assets/images/logo_blanc.png';
    menu = false;

    titre$: Observable<string>;
    isMobile$: Observable<boolean>;
    isProd: boolean;

    constructor(
        private eventRouteService: EventRouteService,
        private store: Store,
        private responsiveService: ResponsiveService,
        private router: Router) {
        this.isProd = environment.production;
    }

    ngOnInit() {

        this.titre$ = this.eventRouteService.getTitre$();
        this.isMobile$ = this.responsiveService.isMobile();

        this.loadGallery();
        this.loadNews();

        this.router.events.subscribe(event => {
            this.closeMenu();
        });
    }

    openMenu(): void {
        this.menu = true;
    }

    closeMenu(): void {
        this.menu = false;
    }

    private loadGallery(): void {
        this.store.dispatch(new LoadGallery());
    }

    private loadNews(): void {
        this.store.dispatch(new LoadNews());
    }
}
