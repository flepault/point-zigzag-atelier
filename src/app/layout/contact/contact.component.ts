import {
  Component,
  OnInit
} from '@angular/core';
import { ResponsiveService } from '../../shared/service/responsive/responsive.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

    isMobile$: Observable<boolean>;

    constructor(private responsiveService: ResponsiveService) {
    }

    ngOnInit(): void {
        this.isMobile$ = this.responsiveService.isMobile();
    }

}
