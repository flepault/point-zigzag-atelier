import { Injectable } from '@angular/core';
import {
    Action,
    Selector,
    State,
    StateContext
} from '@ngxs/store';
import { first } from 'rxjs/operators';
import { Image } from '../../shared/model/image.model';
import { AuthentificationService } from '../../shared/service/authentification/authentification.service';
import { StorageService } from '../../shared/service/storage/storage.service';
import {
    LoadGallery,
    LoadNews,
    Login,
    UpdateGallery,
    UpdateNews
} from './firebase.action';
import {
    LoadGalleryComplete,
    LoadGalleryError,
    LoadNewsComplete,
    LoadNewsError,
    LoginComplete,
    LoginError
} from './firebase.event';


export interface FirebaseStateModel {
    gallery: Array<Image>;
    news: Array<Image>;
}

@State<FirebaseStateModel>({
    name: 'FirebaseState',
    defaults: {
        gallery: [],
        news: []
    }
})
@Injectable()
export class FirebaseState {
    constructor(
        private storageService: StorageService,
        private authentificationService: AuthentificationService
    ) {
    }

    @Selector()
    public static gallery(state: FirebaseStateModel): Image[] {
        return state.gallery;
    }

    @Selector()
    public static news(state: FirebaseStateModel): Image[] {
        return state.news;
    }

    @Action(LoadGallery)
    public loadGallery(
        ctx: StateContext<FirebaseStateModel>
    ) {
        this.storageService
            .getAllImageGalerie()
            .pipe(first())
            .subscribe((images: Image[]) => {
                ctx.patchState({
                    gallery: images.map((image) => ({
                        ...image,
                        selected: false
                    }))
                });
                ctx.dispatch(new LoadGalleryComplete());
            }, err => ctx.dispatch(new LoadGalleryError(err)));
    }

    @Action(UpdateGallery)
    public updateGallery(ctx: StateContext<FirebaseStateModel>, action: UpdateGallery) {
        const state = ctx.getState();
        ctx.patchState({
            gallery: [action.image, ...state.gallery]
        });
    }

    @Action(LoadNews)
    public loadNews(
        ctx: StateContext<FirebaseStateModel>
    ) {
        this.storageService
            .getAllImageNews()
            .pipe(first())
            .subscribe((images: Image[]) => {
                ctx.patchState({
                    news: images.map((image) => ({
                        ...image,
                        selected: false
                    }))
                });
                ctx.dispatch(new LoadNewsComplete());
            }, err => ctx.dispatch(new LoadNewsError(err)));
    }

    @Action(UpdateNews)
    public updateNews(ctx: StateContext<FirebaseStateModel>, action: UpdateNews) {
        const state = ctx.getState();
        ctx.patchState({
            news: [action.image, ...state.news]
        });
    }

    @Action(Login)
    public login(ctx: StateContext<FirebaseStateModel>, action: Login) {
        this.authentificationService.login({email: action.email, password: action.password, returnSecureToken: true})
            .pipe(first())
            .subscribe(() => {
                ctx.dispatch(new LoginComplete());
            }, err => ctx.dispatch(new LoginError(err.error.error.message)));
    }
}
