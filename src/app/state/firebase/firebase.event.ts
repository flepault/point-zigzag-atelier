export class LoadGalleryComplete {
    static readonly type = 'Firebase.loadGalleryComplete';

    constructor() {
    }
}

export class LoadGalleryError {
    static readonly type = 'Firebase.loadGalleryError';

    constructor(public err: string) {
    }
}

export class LoadNewsComplete {
    static readonly type = 'Firebase.loadNewsComplete';

    constructor() {
    }
}

export class LoadNewsError {
    static readonly type = 'Firebase.loadNewsError';

    constructor(public err: string) {
    }
}

export class LoginComplete {
    static readonly type = 'Firebase.loginComplete';

    constructor() {
    }
}

export class LoginError {
    static readonly type = 'Firebase.loginError';

    constructor(public err: string) {
    }
}
