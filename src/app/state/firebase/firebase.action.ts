import { Image } from 'src/app/shared/model/image.model';

export class LoadGallery {
    static readonly type = 'Firebase.loadGallery';

    constructor() {
    }
}

export class UpdateGallery {
    static readonly type = 'Firebase.updateGallery';

    constructor(public image: Image) {
    }
}

export class LoadNews {
    static readonly type = 'Firebase.loadNews';

    constructor() {
    }
}

export class UpdateNews {
    static readonly type = 'Firebase.updateNews';

    constructor(public image: Image) {
    }
}


export class Login {
    static readonly type = 'Firebase.login';

    constructor(public email: string, public password: string) {
    }
}

