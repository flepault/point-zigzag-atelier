import { NgModule } from '@angular/core';
import {
    RouterModule,
    Routes
} from '@angular/router';
import { ConfectionComponent } from './layout/confection/confection.component';
import { WelcomeComponent } from './layout/welcome/welcome.component';
import { ContactComponent } from './layout/contact/contact.component';
import { BroderieComponent } from './layout/broderie/broderie.component';
import { RetoucheComponent } from './layout/retouche/retouche.component';

const confection = 'confection';
const broderie = 'broderie';
const retouche = 'retouche';
const contact = 'contact';

const routes: Routes = [
    {
        path: '',
        data: {
            titre: 'ECRAN.TITRE.ACCEUIL',
        },
        component: WelcomeComponent
    },
    {
        path: confection, data: {
            titre: 'ECRAN.TITRE.CONFECTION',
        },
        component: ConfectionComponent
    },
    {
        path: broderie, data: {
            titre: 'ECRAN.TITRE.BRODERIE',
        },
        component: BroderieComponent
    },
    {
        path: retouche, data: {
            titre: 'ECRAN.TITRE.RETOUCHE',
        },
        component: RetoucheComponent
    },
    {
        path: contact, data: {
            titre: 'ECRAN.TITRE.CONTACT',
        },
        component: ContactComponent
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
