import * as admin from "firebase-admin";

const serviceAccount = require("../key/admin.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://point-zigzag-atelier.firebaseio.com"
});

export const isNotEmptyJson = (jsonObject: Object) =>
    !!jsonObject && Object.keys(jsonObject).length !== 0;
