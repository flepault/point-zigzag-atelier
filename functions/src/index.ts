import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as cors from "cors";
import {
    getAllImageGalerieService,
    getAllImageNewsService,
    uploadImageGalerieService,
    uploadImageNewsService
} from './image';

const corsHandler = cors({origin: true});

//GET ALL IMAGES
export const getAllImageNews = functions.https.onRequest((request, response) => {
    // tslint:disable-next-line: no-empty
    corsHandler(request, response, () => {
        getAllImageNewsService(request, response);
    });
});

export const getAllImageGalerie = functions.https.onRequest((request, response) => {
    // tslint:disable-next-line: no-empty
    corsHandler(request, response, () => {
        getAllImageGalerieService(request, response);
    });
});

//UPLOAD IMAGE NEWS
export const uploadImageNews = functions.https.onRequest((request, response) => {
    corsHandler(request, response, () => {
        let idToken: string | undefined;
        idToken = request.get("Authorization");
        admin.auth().verifyIdToken(!!idToken ? idToken : '')
            .then(() => {
                uploadImageNewsService(request, response);
            }).catch(err => {
            return response.status(500).send(err)
        });
    });
});

//UPLOAD IMAGE GALERIE
export const uploadImageGalerie = functions.https.onRequest((request, response) => {
    corsHandler(request, response, () => {
        let idToken: string | undefined;
        idToken = request.get("Authorization");
        admin.auth().verifyIdToken(!!idToken ? idToken : '')
            .then(() => {
                uploadImageGalerieService(request, response);
            }).catch(err => {
            return response.status(500).send(err)
        });
    });
});

/*

export const migrateImage = functions.https.onRequest((request, response) => {
	// tslint:disable-next-line: no-empty
	corsHandler(request, response, () => {
		admin
		.firestore()
		.collection("image")
		.get()
		.then(snapshot => {
			snapshot.forEach(doc => {

			admin
			.firestore()
			.collection("imageRecette")
			.doc(doc.ref.id)
			.get()
			.then(() => {
				admin
				.firestore()
				.collection("imageRecette")
				.doc(doc.ref.id)
				.set(doc.data())
				.then((data) => 
					{ console.log(data)}
				).catch(reason => {
					response.status(500).send(reason);
				});			

			}).catch(reason => {
				response.status(500).send(reason);
			});
			response.status(200).send();
			});		
		}).catch(reason => {
			response.status(500).send(reason);
		});	
	});
}); */
