import * as functions from "firebase-functions";
import * as path from "path";
import * as os from "os";
import * as fs from "fs";
import * as BusBoy from "busboy";
import * as admin from "firebase-admin";
import * as md5File from "md5-file";
import * as uuid from "uuid";
import {isNotEmptyJson} from './shared';

const app = "point-zigzag-atelier.appspot.com";
const bucket = admin.storage().bucket(app);

export const getAllImageNewsService = (request: functions.https.Request, response: functions.Response) => {

    const filesUrl: any[] = [];
    admin
        .firestore()
        .collection("imageNews")
        .orderBy("dateCreation", "desc")
        .limit(5)
        .get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                if (isNotEmptyJson(doc.data())) {
                    filesUrl.push(doc.data());
                }
            });
            response.status(200).send(filesUrl);
        })
        .catch(reason => {
            response.status(500).send(reason);
        });
}


export const getAllImageGalerieService = (request: functions.https.Request, response: functions.Response) => {

    const filesUrl: any[] = [];
    admin
        .firestore()
        .collection("imageGalerie")
        .orderBy("dateCreation", "desc")
        .get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                if (isNotEmptyJson(doc.data())) {
                    filesUrl.push(doc.data());
                }
            });
            response.status(200).send(filesUrl);
        })
        .catch(reason => {
            response.status(500).send(reason);
        });
}

export const uploadImageNewsService = (request: functions.https.Request, response: functions.Response) => {
    const busboy = new BusBoy({headers: request.headers});
    const imageToBeUploaded = {imageFileName: "", tmpFilePath: "", filePath: "", mimetype: "", imageType: "news"};

    uploadImage(busboy, response, imageToBeUploaded);
    finishUploadImageNews(busboy, response, imageToBeUploaded);
    busboy.end(request.rawBody);
}

export const uploadImageGalerieService = (request: functions.https.Request, response: functions.Response) => {
    const busboy = new BusBoy({headers: request.headers});
    const imageToBeUploaded = {imageFileName: "", tmpFilePath: "", filePath: "", mimetype: "", imageType: "galerie"};

    uploadImage(busboy, response, imageToBeUploaded);
    finishUploadImageGalerie(busboy, response, imageToBeUploaded);
    busboy.end(request.rawBody);
}

const uploadImage = (
    busboy: any,
    response: functions.Response,
    imageToBeUploaded: any
) => {
    busboy.on(
        "file",
        (
            fieldname: any,
            file: any,
            filename: any,
            encoding: any,
            mimetype: any
        ) => {
            if (mimetype !== "image/jpeg" && mimetype !== "image/png") {
                return response
                    .status(400)
                    .json({error: "Wrong file type submitted"}).send();
            }
            // my.image.png => ['my', 'image', 'png']
            const imageExtension = filename.split(".")[
            filename.split(".").length - 1
                ];
            // 32756238461724837.png
            const imageFileName = `${Math.round(
                Math.random() * 1000000000000
            ).toString()}.${imageExtension}`;

            imageToBeUploaded.filePath = path.join(imageToBeUploaded.imageType, '/', imageFileName);
            imageToBeUploaded.tmpFilePath = path.join(os.tmpdir(), imageFileName);
            imageToBeUploaded.mimetype = mimetype;
            imageToBeUploaded.imageFileName = imageFileName;

            file.pipe(fs.createWriteStream(imageToBeUploaded.tmpFilePath));
            return;
        }
    );
}

const finishUploadImageNews = (busboy: any, response: functions.Response, imageToBeUploaded: any) => {
    busboy.on("finish", () => {
        const UUID = uuid.v4();

        const hash = md5File.sync(imageToBeUploaded.tmpFilePath);

        admin.firestore()
            .collection("imageNews")
            .doc(hash)
            .get().then(snap => {

            if (snap.exists) {
                return response.json(snap.data()).send();
            } else {
                bucket.upload(imageToBeUploaded.tmpFilePath, {
                    destination: imageToBeUploaded.filePath,
                    resumable: false,
                    metadata: {
                        metadata: {
                            contentType: imageToBeUploaded.mimetype,
                            firebaseStorageDownloadTokens: UUID,
                            md5Hash: hash
                        }
                    }
                })
                    .then(() => {
                        const url = `https://firebasestorage.googleapis.com/v0/b/${app}/o/${imageToBeUploaded.imageType}%2F${imageToBeUploaded.imageFileName}?alt=media&token=${UUID}`;

                        uploadThumbnailImage(imageToBeUploaded.imageFileName, imageToBeUploaded.filePath, imageToBeUploaded.mimetype, 750)
                            .then((thumbnailUrl_750: string) => {

                                admin.firestore()
                                    .collection("imageNews")
                                    .doc(hash).get()
                                    .then(() => {
                                        const image = {
                                            url: thumbnailUrl_750,
                                            fullsizeUrl: url,
                                            dateCreation: admin.firestore.FieldValue.serverTimestamp()
                                        };
                                        admin
                                            .firestore()
                                            .collection("imageNews")
                                            .doc(hash)
                                            .set(image)
                                            .then(() => {
                                                return response
                                                    .json(image)
                                                    .send();
                                            })
                                            .catch(err => {
                                                return response.status(500).send(err)
                                            });
                                    })
                                    .catch(err => {
                                        return response.status(500).send(err)
                                    });
                            })
                            .catch(err => {
                                return response.status(500).send(err)
                            });
                    })
                    .catch(err => {
                        return response.status(500).send(err)
                    });
                return;
            }
        }).catch(err => {
            return response.status(500).send(err)
        });
    });
}

const finishUploadImageGalerie = (busboy: any, response: functions.Response, imageToBeUploaded: any) => {
    busboy.on("finish", () => {
        const UUID = uuid.v4();

        const hash = md5File.sync(imageToBeUploaded.tmpFilePath);

        admin.firestore()
            .collection("imageGalerie")
            .doc(hash)
            .get().then(snap => {

            if (snap.exists) {
                return response.json(snap.data()).send();
            } else {
                bucket.upload(imageToBeUploaded.tmpFilePath, {
                    destination: imageToBeUploaded.filePath,
                    resumable: false,
                    metadata: {
                        metadata: {
                            contentType: imageToBeUploaded.mimetype,
                            firebaseStorageDownloadTokens: UUID,
                            md5Hash: hash
                        }
                    }
                })
                    .then(() => {
                        const url = `https://firebasestorage.googleapis.com/v0/b/${app}/o/${imageToBeUploaded.imageType}%2F${imageToBeUploaded.imageFileName}?alt=media&token=${UUID}`;

                        uploadThumbnailImage(imageToBeUploaded.imageFileName, imageToBeUploaded.filePath, imageToBeUploaded.mimetype, 200)
                            .then((thumbnailUrl_200: string) => {

                                admin.firestore()
                                    .collection("imageGalerie")
                                    .doc(hash).get()
                                    .then(() => {
                                        const image = {
                                            url: url,
                                            thumbnailUrl: thumbnailUrl_200,
                                            dateCreation: admin.firestore.FieldValue.serverTimestamp(),
                                        };
                                        admin
                                            .firestore()
                                            .collection("imageGalerie")
                                            .doc(hash)
                                            .set(image)
                                            .then(() => {
                                                return response
                                                    .json(image)
                                                    .send();
                                            })
                                            .catch(err => {
                                                return response.status(500).send(err)
                                            });
                                    })
                                    .catch(err => {
                                        return response.status(500).send(err)
                                    });
                            })
                            .catch(err => {
                                return response.status(500).send(err)
                            });
                    })
                    .catch(err => {
                        return response.status(500).send(err)
                    });
                return;
            }
        }).catch(err => {
            return response.status(500).send(err)
        });
    });
}

//UPLOAD THUMBNAIL IMAGE
const uploadThumbnailImage = async (fileName: string, filePath: string, contentType: string, size: number) => {

    const spawn = require('child-process-promise').spawn;

    const UUID = uuid.v4();

    const tempFilePath = path.join(os.tmpdir(), `tmp_${size}_${fileName}`);

    const metadata = {
        metadata: {
            contentType: contentType,
            firebaseStorageDownloadTokens: UUID
        }
    }

    await bucket.file(filePath).download({destination: tempFilePath});
    // Generate a thumbnail using ImageMagick.
    await spawn('convert', [tempFilePath, '-thumbnail', `${size}x${size}>`, tempFilePath]);
    // We add a 'thumb_' prefix to thumbnails file name. That's where we'll upload the thumbnail.
    const thumbFileName = `thumb_${fileName}_${size}`;
    const thumbFilePath = path.join(path.dirname(filePath), thumbFileName);
    // Uploading the thumbnail.
    await bucket.upload(tempFilePath, {
        destination: thumbFilePath,
        resumable: false,
        metadata: metadata,
    });
    const thumbnailUrl = `https://firebasestorage.googleapis.com/v0/b/${app}/o/${path.dirname(filePath)}%2F${thumbFileName}?alt=media&token=${UUID}`;
    // Once the thumbnail has been uploaded delete the local file to free up disk space.
    fs.unlinkSync(tempFilePath);

    return thumbnailUrl;
};
